#
# Compiler flags
#

# NOTE : Physfs breaks require in lua . will fix 

CC     = gcc

ifeq ($(OS),Windows_NT)

	# platform specific flags , link, and name
	PLAT_LD = -lws2_32  -march=x86-64 -mconsole -lmingw32
	PLAT_NAME = test.exe
  RUBY_FLAGS = -Iruby-2.4.0 -Iruby-2.4.0/x64-mingw32 -DCRUBY
  RUBY_LD = -lx64-msvcrt-ruby240.dll
  RUBY_SOURCE = 

else

	# platform specific flags , link, and name
	PLAT_FLAGS = -Iusr/include/SDL2
	PLAT_LD = -march=x86-64 -lm -lz -ldl -lpthread -lgmp -lcrypt
	PLAT_NAME = test

	RUBY_FLAGS = -DCRUBY $(shell pkg-config ruby --cflags)
	RUBY_LD = -L/usr/lib/x86_64-linux-gnu $(shell pkg-config ruby --libs)
	RUBY_SOURCE =

endif


CFLAGS = -g -O3 -c $(RUBY_FLAGS)
X64_LDFLAGS=  $(RUBY_LD)
SOURCES= embeddedruby.c

OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=$(PLAT_NAME)
all: test

clean: 
	rm -f $(OBJECTS) $(PLAT_NAME)

test: $(OBJECTS)
	$(CC) $(OBJECTS) $(X64_LDFLAGS) -o $(PLAT_NAME)

.c.o:
	$(CC) $(CFLAGS) $< -o $@

