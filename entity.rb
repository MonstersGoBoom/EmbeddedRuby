# Entity
# everything 
class Entity
  attr_accessor :id, :x, :y, :vx, :vy

  @list = []

  class << self # This is how you access class instance variables
    attr_accessor :list
  end

  # set up the variables
  def initialize
    @id = rand(0...30)
    @x = rand(0...100)
    @y = rand(0...100)
    @vx = rand(-1.0...1.0)
    @vy = -rand(0...1.0)
    Entity.list.push(self)
  end

  # do rendering
  def draw
  end

  # do update
  def think
    @x += @vx
    @y += @vy
    @vy += 0.05
  end
end
